import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatbotComponent } from './chatbot/chatbot.component';


// console.log('app routing')
const routes: Routes = [
  // { path: '', redirectTo: 'chatbot', pathMatch: 'full' },
  //{ path: 'chatbot', component: ChatbotComponent },
  { path: 'modules', loadChildren: './modules/modules.module#ModulesModule' },
  //{path:'services',loadChildren:'./modules/modules.module#ModulesModule'  },
  { path: '**', redirectTo: '/modules/home' , pathMatch: 'full'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

