import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { log } from 'util';


@Injectable()
export class NewChatbotService {

  // public url = 'http://150.107.240.144:5504';41.160.253.185
  // public url = 'http://192.168.0.89:5506';
  // public IP  http://41.160.253.186/
  public url = 'http://41.160.253.186:5506';
  // public url = 'http://192.168.0.89:5506';
  // public url = 'http://182.76.238.199:5502';
  public socket;
  public catagoryName: any = [];
  public subCatagoryList = "";
  public objDate = Date.now();

  sendMessage(message) {
    this.socket.send(message);
  }

  sendInActiveMessage(message) {
    this.socket.emit('sendInActive', message);
  }

  statusyes(data) {
    this.socket.emit('message', data);
  }
  startoverNo(data) {
    this.socket.emit('message', data);
  }
  startover(message) {
    this.socket.emit('startover', message);
  }

  sendMessageJOSN(obj) {
    this.socket.emit('jsonticketopen', obj);
  }

  sendJsonTicketReopen(obj) {
    this.socket.emit('jsonticketreopen', obj);
  }

  sendJsoncustomerComments(obj) {
    this.socket.emit('jsoncustomerquery', obj);
  }

  sendJsonTeamDetails(obj) {
    this.socket.emit('showteamdetail', obj);
  }

  sendJsonapllyleave(obj) {
    this.socket.emit('applyleave', obj);
  }

  getConnection() {
    console.log('print chatbot service');
    this.socket = io(this.url, {
      query: {
        username: "Vaibhav",
        data: "Test User"
      }
    });
  }

  getMessages() {
    let observable = new Observable(observer => {
      this.socket.on('message', (data) => {
        data['serType'] = 'normal';
        observer.next(data);
      });

      this.socket.on('messagebot', (data) => {
        data['serType'] = 'normal';
        console.log("messagebot ::: " + JSON.stringify(data));
        observer.next(data);
      });

      this.socket.on('getuifields', (data) => {
        data['serType'] = 'c_inci';
        observer.next(data);
      });

      this.socket.on('ticket_cat_subcat', (data) => {
        data['serType'] = 'ticket_cat_subcat';
        data['serType'] = 'ticket_cat_subcat';
        data['dataStr'] = data;
        observer.next(data);
      });

      this.socket.on('loading', (data) => {
        data['serType'] = 'loading';
        observer.next(data);
      });
    });
    return observable;
  }

  public disconnectBot() {
    this.socket.disconnect();
  }

}
