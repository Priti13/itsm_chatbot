import { Observable, Subscription } from 'rxjs/Rx';
import { Component, OnInit, EventEmitter, Renderer, HostListener, AfterViewInit, OnChanges, ElementRef } from '@angular/core';
import { Router } from '@angular/router'
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { ChatbotService } from "./chatbot.service";
import * as $ from "jquery";
import { SpeechRecognitionService } from '../speech-recognition/speech-recognition.service';
import themeConf_ from '../config/theme-settings';
import { NgbModalRef, NgbModal, ModalDismissReasons, NgbPaginationModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';




@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./css/stylenewchatbot.css']
})
export class ChatbotComponent implements OnInit, AfterViewInit, OnChanges {


  public date = {
    start: {
      d: "",
      m: "",
      y: "2019"
    },
    end: {
      d: "",
      m: "",
      y: "2019"
    },
    month:[],
    days:[],
    years:[]
  }

  public isloading: boolean = false;

  public messDelay: number = 1300;
  public inactiveSince: number = 0;
  public subscrNotifyInactivity: Subscription;

  public catSubCatStr: any = {};

  public interval$ = null;
  public intervalCards$ = null;

  public isConnected: boolean = false;

  public lastMessRes_: any = {};

  public modMess_ = { userName: "", isAuthorize: true, isAudio: false, content: [], custCards: [], showCarts: false, typingEnable: true, notifyActivity: false, maxInactiveLimit: 60 };
  public chttxt_plcholder: string = "Type Your Message ....";
  public voices: SpeechSynthesisVoice[] = [];

  public formSubmission(obj, ndx, action) {
    //console.log("customercommentfunction " + JSON.stringify(obj));
    if (action && action === 'ticketopenfunction') {
      this.jsonticketopen(obj);
      this.AbortSpeak();
    } else if (action && action === 'startover') {
      this.exitfunction(obj);
      this.AbortSpeak();
    } else if (action && action === 'statusyes') {
      this.statusyes(obj, ndx, action);
      this.AbortSpeak();
    } else if (action && action === 'ticketreopenfunction') {
      this.jsonticketreopen(obj, ndx, action);
      this.AbortSpeak();
    } else if (action && action === 'customercommentfunction') {
      this.jsoncustomerquery(obj, ndx, action);
      this.AbortSpeak();
    }
    else if (action && action === 'teaminfofunction') {
      this.teamdetails(obj, ndx, action);
      this.AbortSpeak();
    }
    else if (action && action === 'applyleavefunction') {
      this.applyleave(obj);
      this.AbortSpeak();
    }

  }

  private applyleave(obj) {
    console.log("apply leave : " + JSON.stringify(obj));
    var template = {}
    var fields: any[] = obj.fields;
    fields.forEach(fld => {

      if (fld.type == 'date:startdate') {
        fld.model = this.date.start.y + "-" + this.date.start.m + "-" + this.date.start.d + " " + "00:00:00";
      } else if (fld.type == 'date:enddate') {
        fld.model = this.date.end.y + "-" + this.date.end.m + "-" + this.date.end.d + " " + "23:59:59";
      }

      if (this.validFormFType.indexOf(fld.type) > -1) {
        var vmodl_emailText = fld.model;
        var id = fld.id;
        template[id] = vmodl_emailText;
      }
    })

    console.log("submit leave  : " + JSON.stringify(template));
    this.chatService.sendJsonapllyleave(template);
    this.chatService.sendMessage('Submited Leave')
    this.stuckform(obj);
    this.scrollSet();
    this.resetDate();
  }

  resetDate() {
    this.date.start.m = "";
    this.date.start.d = "";
    this.date.start.y = "2019";
    this.date.end.m = "";
    this.date.end.d = "";
    this.date.end.y = "2019";
  }

  private teamdetails(obj, ndx, action) {
    var template = {}
    var fields: any[] = obj.fields;
    fields.forEach(fld => {
      if (this.validFormFType.indexOf(fld.type) > -1) {
        var vmodl_emailText = fld.model;
        var id = fld.id;
        template[id] = vmodl_emailText;
      }
    })
    this.chatService.sendJsonTeamDetails(template);
    this.usrtoMachine('Show team details');
    this.stuckform(obj);
    this.scrollSet();
  }


  private jsoncustomerquery(obj, ndx, action) {
    var template = {}
    var fields: any[] = obj.fields;
    fields.forEach(fld => {
      if (this.validFormFType.indexOf(fld.type) > -1) {
        var vmodl_emailText = fld.model;
        var id = fld.id;
        template[id] = vmodl_emailText;
      }
    })
    this.chatService.sendJsoncustomerComments([template]);
    this.usrtoMachine('Customer Comment');
    this.stuckform(obj);
    this.scrollSet();
  }

  private jsonticketreopen(obj, ndx, action) {
    var template = {}
    var fields: any[] = obj.fields;
    fields.forEach(fld => {
      if (this.validFormFType.indexOf(fld.type) > -1) {
        var vmodl_emailText = fld.model;
        var id = fld.id;
        template[id] = vmodl_emailText;
      }
    })
    this.chatService.sendJsonTicketReopen([template]);
    this.usrtoMachine('Reopen Ticket');
    this.stuckform(obj);
    this.scrollSet();
  }


  public statusyes(obj, ndx, action) {
    // this.sendMessToService('yes-'+obj.model);
    let objFld = obj.fields[ndx];
    this.chatService.sendMessage('yes' + objFld.model)
    //this.usrtoMachine('yes');
    this.stuckform(obj);
    this.scrollSet();
  }

  private exitfunction(obj) {
    this.chatService.startover('startover');
    // this.usrtoMachine('exit');
    this.stuckform(obj);
    this.clearChatWindow();
    this.scrollSet();
  }

  private jsonticketopen(obj) {
    var template = {}
    var fields: any[] = obj.fields;
    fields.forEach(fld => {
      if (this.validFormFType.indexOf(fld.type) > -1) {
        var vmodl_emailText = fld.model;
        var id = fld.id;
        template[id] = vmodl_emailText;
      }
    })

    this.chatService.sendMessageJOSN([template]);
    this.chatService.sendMessage('')
    this.stuckform(obj);
    this.scrollSet();
  }

  public validFormFType = ['input:text', 'input:number', 'input:email', 'select','date:startdate','date:enddate'];
  public modCreateIncident = {
    mappingcat: [],
    customerServiceMapp: []
  }


  ngAfterViewInit() {
    this.loadJSOnInit();
  }

  public chat

  public toggleCh() {
    this.clearChatWindow();
    this.isConnected = !this.isConnected;
    if (!this.isConnected) {
      this.chatDisconnected();
      this.chatService.disconnectBot();
    } else {
      this.isloading = true;
      this.initSpeak();
    }
    if ($('#In2CB_chatdiv').hasClass('close')) {
      $('#In2CB_chatdiv').removeClass('close');
      $('#In2CB_chatdiv').addClass('open');
      this.openChat();
      this.scrollSet();
      this.voices = window.speechSynthesis.getVoices();
    } else if ($('#In2CB_chatdiv').hasClass('open')) {
      $('#In2CB_chatdiv').removeClass('open');
      $('#In2CB_chatdiv').addClass('close');
      this.clearChatWindow();
      this.disconnect();
    }
  }

  public chatDisconnected() {
    this.clearMessageInterval();
    this.ClearIntervalCards();
    this.scrollSet();
    this.AbortSpeak();
  }


  async loadJSOnInit() {
    await this.loadScript('assets/chatbot/js/jquery.min.js');
    await this.loadScript('assets/chatbot/js/index.js');
    await this.loadScript('assets/chatbot/js/jquery-2.2.4.min.js');
  }

  public loadScript(scriptUrl: string) {
    return new Promise((resolve, reject) => {
      const scriptElement = document.createElement('script');
      scriptElement.src = scriptUrl;
      scriptElement.onload = resolve;
      document.body.appendChild(scriptElement);
    })
  };

  public boatCardClk(crdObj_) {
    var messtoSend = crdObj_.action;
    var messtoDisplay = crdObj_.displayMess;
    this.sendMessToService(messtoSend);
    this.usrtoMachine(messtoDisplay);
    this.modMess_.showCarts = !this.modMess_.showCarts;
    this.scrollSet();
    this.AbortSpeak();
  }


  /**
   * 
   * @param sel 
   * @param obj 
   * @param obsec 
   * @param action 
   */
  public onDynamicDDChange(sel, obj, obsec, action) {
    if (action && action == 'selectCategoryid') {
      obj.forEach(element => {
        if (element.id == 'subcategory') {
          element['options'] = this.catSubCatStr[sel];
        }
        return;
      });
    } else if (action && action == 'selectcatagorycreateincident') {
      let sCatDD = this.getSubCateCreateIncidnt(sel);
      for (var i = 0; i < obj.length; i++) {
        if (obj[i].id === 'subcatagory') {
          obj[i].options = sCatDD;
        }
      }
    }

    else if (action && action == 'selectserviceid') {
      let cSerDD = this.getServiceIdCreateIncident(sel.toLowerCase());
      for (var i = 0; i < obj.length; i++) {
        if (obj[i].id === 'serviceId') {
          obj[i].options = cSerDD;
        }
      }
    }
  }


  public getServiceIdCreateIncident(name): any[] {
    let serviceIds = [{
      key: "Select Service Id", value: ""
    }]
    for (var i = 0; i < this.modCreateIncident.customerServiceMapp.length; i++) {
      if (this.modCreateIncident.customerServiceMapp[i].customerName === name) {
        for (var j = 0; j < this.modCreateIncident.customerServiceMapp[i].serviceIdList.length; j++) {
          serviceIds.push(this.modCreateIncident.customerServiceMapp[i].serviceIdList[j]);
        }
        return serviceIds;
      }
    }
    return serviceIds;
  }


  public getSubCateCreateIncidnt(name): any[] {
    let subCate = [{
      key: "Select Sub-Category", value: ""
    }]
    for (var i = 0; i < this.modCreateIncident.mappingcat.length; i++) {
      if (this.modCreateIncident.mappingcat[i].catagoryName === name) {
        for (var j = 0; j < this.modCreateIncident.mappingcat[i].subCatagoryList.length; j++) {
          subCate.push({
            key: this.modCreateIncident.mappingcat[i].subCatagoryList[j],
            value: this.modCreateIncident.mappingcat[i].subCatagoryList[j]
          })
        }
        return subCate;
      }
    }
    return subCate;
  }


  public stuckform(obj) {
    var fields: any[] = obj.fields;
    fields.forEach(fld => {
      if (fld.hasOwnProperty('editable')) {
        fld.editable = false;
      }
      if (fld.hasOwnProperty('required')) {
        fld.required = false;
      }
    })

  }

  public mousedown = new EventEmitter<MouseEvent>();
  public speaking: boolean = false;
  public showTabBtn = false;
  public currentChatHtml = "";
  public c_ = 0;
  public messages = [];
  public connection;
  public temp;
  public idList: any = [];
  public speechData: string;
  public speeker = true;
  public showCarts = true;

  scrollSet() {
    var container = $('#In2CB_chatBoxMainContainer')[0];
    var containerHeight = container.clientHeight;
    var contentHeight = container.scrollHeight;
    let scrollupTo = $(".messages")[0].scrollHeight;
    let idlen = $(".messages")[0].scrollHeight;
    $("#In2CB_chatBoxMainContainer").stop().animate({ scrollTop: $("#In2CB_chatBoxMainContainer")[0].scrollHeight }, 1000);
  }


  themeConf_;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private http: Http,
    private chatService: ChatbotService,
    private renderer: Renderer,
    private speechRecognitionService: SpeechRecognitionService

  ) {
    this.speechData = "";
    this.isConnected = false;

    let arr = Array(11).fill(1).map((x,i)=>i+1);
    console.log("arr : "+arr);

  }


  public sendMessage() {
    var msztype = $('#myMessage').val();
    // console.log(msztype)
    // return
    if (msztype.trim() != "") {
      this.usrtoMachine(msztype);
      $('#myMessage').val('');
      this.sendMessToService(msztype);
    }
  }

  public sendMessToService(mess_) {
    this.chatService.sendMessage(mess_);
    this.scrollSet();
  }
  public chatData_ = { "isAudio": true, "typingEnable": false, "showCarts": false, "content": [{ "type": "form", "formCss": "messageBody ui-chatbox-msg ticketno chat-bubble user-chat-bubble text-bubble In2\r\n  _width300p", "src": "mcn", "name": "show ticket details", "onSubmit": "", "id": "1", "fields": [{ "labelName": "", "type": "mess:keyValue", "placeholder": "", "model": "", "value": "", "options": [{ "days": "1", "leave_type": "SL_2019", "status": "APPROVED", "comment": "Today I am not feeling well, having fever." }, { "days": "1", "leave_type": "SL", "status": "APPR\r\n  OVED", "comment": "I was feeling sick, I had a journey on previous night (25-dec). so I was not comfortable to come office on 26-dec, Please approved my sick leave.\nThank you" }, { "d\r\n  ays": "1", "leave_type": "2019", "status": "APPROVED", "comment": "I had urgent work with my family, so I was not come to office.\n\nThank you" }, { "days": "2", "leave_type": "2019", "status": "CANCELLED", "comment": "bhai dhuj and gavarghan pujan.." }, { "days": "1", "leave_type": "2019", "status": "APPROVED", "comment": "i was not feeling well." }, { "days": "3", "\r\n  leave_type": "SL_2019", "status": "APPROVED", "comment": "major injury in left hand figure." }, { "days": "1", "leave_type": "SL_2019", "status": "APPROVED", "comment": "right hand fig\r\n  ure broken injury." }, { "days": "2", "leave_type": "ML_2019", "status": "APPROVED", "comment": "Bhai dujj and govardan pujan." }], "cssclass": "col-sm-12", "editable": true, "required": true, "id": "subjects" }] }] };
  ngOnInit() {
    this.themeConf_ = themeConf_;

    console.log('ChatData------' + JSON.stringify(this.chatData_.content['options']))
  }


  public startNotifyInactivity() {
    this.inactiveSince = 0;
    let timer = Observable.timer(1000, 1000);
    this.subscrNotifyInactivity = timer.subscribe(t => {
      this.getsubscrNotifyInactivity();
    });

  }

  public getsubscrNotifyInactivity() {
    if (this.modMess_.notifyActivity) {
      this.inactiveSince += 1;
      if (this.inactiveSince >= this.modMess_.maxInactiveLimit) {
        this.notifyLimitExceedsOpr();
      }
    } else {
      this.inactiveSince = 0;
      this.stopNotifyInactivity();
    }
  }

  public notifyLimitExceedsOpr() {
    if (this.inactiveSince == 15) {
      //send first message
      let inactive_warning1 = {
        userName: "", isAuthorize: true, isAudio: true, content: [
          {
            "src": "mcn", "type":
              "textmessage", "value": 'So have you decided your answer or should i proceeds'
          }
        ], custCards: [], showCarts: false, typingEnable: false, notifyActivity: true, maxInactiveLimit: 60
      };
      this.machineToUser(inactive_warning1);
    } else if (this.inactiveSince == 25) {
      this.chatService.sendInActiveMessage("inactive since 25 secounds");
      if ((this.modMess_.content.length > 1) && (this.modMess_.content[this.modMess_.content.length - 2].type == 'form')) {
        this.stuckform(this.modMess_.content[this.modMess_.content.length - 2]);
      }
    } else if (this.inactiveSince > 26) {
      this.stopNotifyInactivity();
    }
  }

  public stopNotifyInactivity() {
    if (!this.subscrNotifyInactivity.closed) {
      this.subscrNotifyInactivity.unsubscribe();
    }
  }



  public socket(message) {
    this.machineToUser(message);
    this.scrollSet();
  };

  ngOnDestroy() {
    this.AbortSpeak();
    this.clearChatWindow();
    this.speechRecognitionService.DestroySpeechObject();
    this.chatBotSpeak('');
    this.isloading = false;
  }

  public disconnect() {
    this.chatService.disconnectBot();
  }


  public chatBotSpeak(message) {
    if (this.speeker && this.isConnected) {
      if ('speechSynthesis' in window) {
        var msg = new SpeechSynthesisUtterance(message);
        this.voices = window.speechSynthesis.getVoices();
        if (this.voices.length > 0) {
          msg.voice = this.voices[3];
          msg.rate = 1;
        }
        window.speechSynthesis.speak(msg);
      }
    }
  }

  public initSpeak() {
    if ('speechSynthesis' in window) {
      var msg = new SpeechSynthesisUtterance('');
      this.voices = window.speechSynthesis.getVoices();
      if (this.voices.length > 0) {
        msg.voice = this.voices[3];
        msg.volume = 12;
        msg.rate = 1;
      }
      window.speechSynthesis.speak(msg);
    }
  }


  public AbortSpeak() {
    if (this.speeker) {
      window.speechSynthesis.cancel();
    }
  }

  public openChatWindow() {
    $('#qnimate').addClass('popup-box-on');
    $("#removeClass").click(function () {
      $('#qnimate').removeClass('popup-box-on');
    });
    this.showTabBtn = !this.showTabBtn;
  }




  public usrtoMachine(message) {
    this.modMess_.content.push({
      src: "usr",
      type: "textmessage",
      value: message
    });
    this.scrollSet();
    return this.currentChatHtml;
  }


  public counterTemp = 0;

  public machineToUser(message) {
    this.counterTemp = 0;

    if (message && message.content) {
      this.validateInterval(message);
    }
    this.scrollSet();
    return this.currentChatHtml;
  }

  public validateInterval(message) {
    let count = 0;
    if (message.content && message.content.length > 0) {
      const interval = window.setInterval(() => {
        this.manageLoader(message, count);
        count++;
        this.isloading = true;
        if ((count >= message.content.length) || (!this.isConnected)) {
          this.isloading = false;
          window.clearInterval(interval);
        }
      }, this.messDelay);
    }

  }

  public clearMessageInterval() {
    if (this.interval$ && this.interval$ != null) {
      window.clearInterval(this.interval$);
    }
  }


  public ClearIntervalCards() {
    if (this.intervalCards$ && this.intervalCards$ != null) {
      window.clearInterval(this.intervalCards$);
    }
  }

  public manageLoader(message, i) {
    this.isloading = false;
    if (this.speeker && message.isAudio && message.content[i].type === 'textmessage') {
      if (this.isConnected) {
        this.chatBotSpeak(message.content[i].value);
      } else {
        console.log("Can't Connect");
      }

    }
    this.modMess_.content.push(message.content[i]);
    this.scrollSet();
  }

  public manageIndicator(sts) {
    if (sts == 'close') {
      this.isloading = false;
    } else if (sts == 'open') {
      this.isloading = true;
    }
  }

  public clearChatWindow() {
    this.modMess_ = { userName: "", isAuthorize: true, isAudio: false, content: [], custCards: [], showCarts: false, typingEnable: false, notifyActivity: false, maxInactiveLimit: 60 };
  }

  public handleKeyDown(event: any) {
    if (event.keyCode == 13) {
      this.sendMessage();
    }
  }

  public openChat() {
    this.showTabBtn = !this.showTabBtn;
    this.openChatWindow();
    this.connection = this.chatService.getConnection();
    this.chatService.getMessages().subscribe(message => {
      this.validateMsz(message);
    })

  }


  public validateMsz(message) {
    this.modMess_.notifyActivity = message.notifyActivity;
    this.modMess_.maxInactiveLimit = message.maxInactiveLimit;
    if (this.modMess_.notifyActivity) {
      this.startNotifyInactivity();
    }

    if (message.serType == 'loading') {
      if (message.duration && message.duration > 0) {
        this.manageIndicator('open');
        setTimeout(() => {
          this.manageIndicator('open');
        }, ((this.messDelay) + 10) * (message.duration));
      } else {
        let lastContents = (this.lastMessRes_.content && this.lastMessRes_.content.length > 0) ? this.lastMessRes_.content.length : 1;
        this.manageIndicator('open');
        setTimeout(() => {
          this.manageIndicator('open');
        }, (((this.messDelay) + 10) * (lastContents)));
      }
    } else if (message.serType == 'ticket_cat_subcat') {

      this.catSubCatStr = message.dataStr;
    }
    else if (message.serType == 'c_inci') {
      this.modCreateIncident.mappingcat = message.mapping;
      this.modCreateIncident.customerServiceMapp = message.customerServiceMapping;

      this.modMess_.typingEnable = message.typingEnable;
      this.lastMessRes_ = message;
      this.messages.push(message);
      this.socket(message);
    } else if (message.custCards && message.custCards.length > 0 && message.showCarts) {
      let lastContents = (this.lastMessRes_.content && this.lastMessRes_.content.length > 0) ? this.lastMessRes_.content.length : 1;
      this.modMess_.typingEnable = message.typingEnable;

      this.intervalCards$ = window.setInterval(() => {
        if (this.isConnected) {
          this.modMess_.custCards = message.custCards;
          this.modMess_.showCarts = message.showCarts;
          this.scrollSet();
        }
        window.clearInterval(this.intervalCards$);

      }, ((this.messDelay) * (lastContents + 2)));

      this.lastMessRes_ = message;
      this.messages.push(message);
      this.socket(message);
    } else if (message.serType == 'normal') {
      this.modMess_.typingEnable = message.typingEnable;
      this.modMess_.showCarts = message.showCarts;
      this.lastMessRes_ = message;
      this.messages.push(message);
      this.socket(message);
    }

  }

  public closeChat() {
    this.showTabBtn = !this.showTabBtn;
    this.idList = [];
    this.connection.unsubscribe();
  }

  public onChange(value: any) {
  }

  public makeMicrophoneBlink(sts) {
    if (sts) {
      if (!($('#microphone').hasClass('speak'))) {
        $('#microphone').addClass('speak');
      }
    } else {
      if ($('#microphone').hasClass('speak')) {
        $('#microphone').removeClass('speak');
      }
    }
  }

  activeSpeechSearch(): void {
    this.chttxt_plcholder = "Please Speak ....."
    this.makeMicrophoneBlink(true);
    this.speechRecognitionService.record()
      .subscribe(
        (value) => {
          this.chttxt_plcholder = "Bot is Listining....."
          this.speechData = value;
          if (this.speechData && (this.speechData != "")) {
            this.usrtoMachine(this.speechData);
            this.chatService.sendMessage(this.speechData);
          }
        },
        //errror
        (err) => {
          if (err.error == "no-speech") {
            this.activeSpeechSearch();
          }
          this.chttxt_plcholder = "Can't Recognise Your Voice ...";
        },
        //completion
        () => {
          this.chttxt_plcholder = "Type Your Message ....";
          this.makeMicrophoneBlink(false);
        });
  }

  @HostListener('mouseDown', ['$event'])
  mouseDown(event) {
    this.activeSpeechSearch();
  }


  public toggleVoiceMute() {
    this.speeker = !(this.speeker);
    if (!this.speeker) {
      window.speechSynthesis.cancel();
    }
  }


  public getMessageLiCSS(obj) {
    if (obj.src == 'usr') {
      return 'message col-xs-9 pull-right ';
    } else if (obj.src == 'mcn') {
      return 'message col-xs-9 pull-left  ';
    }
  }

  ngOnChanges() { }

  public manageMszBoxDiv() {
    let position = 0;
    if (this.modMess_.showCarts && this.modMess_.custCards.length > 0) {
      let shand = document.getElementsByClassName('inputContainter') as HTMLCollectionOf<HTMLElement>;
      if (shand.length != 0) {
        shand[0].style.bottom = "0px";
      }
    } else {
      let shand = document.getElementsByClassName('inputContainter') as HTMLCollectionOf<HTMLElement>;
      if (shand.length != 0) {
        shand[0].style.bottom = "0px";
      }
    }

    return true;
  }


  onSingleSelect(event, flds) {
  }

  onSingleDeSelect(event) {
  }
  onSelectAll(event) {
  }
  onDeSelectAll(event) {
  }


}
