import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { log } from 'util';


@Injectable()
export class ChatbotService {

  // public url = 'http://172.27.63.162:5501';
  public url = 'http://172.16.11.96:5503';
  // public url = 'http://172.16.10.56:8080';
  // public url = 'http://182.76.238.199:5502';
  public socket;
  public catagoryName: any = [];
  public subCatagoryList = "";
  public objDate = Date.now();

  sendMessage(message) {
    console.log('here service');
    console.log(message);
    this.socket.send(message);
  }


  sendInActiveMessage(message) {
    console.log("sendInActiveMessage call")
    this.socket.emit('sendInActive', message);
  }

  startover(message) {
    this.socket.emit('startover', message);
  }

  sendMessageJOSN(obj) {
    this.socket.emit('jsonticketopen', obj);

  }

  sendJsonTicketReopen(obj) {
    this.socket.emit('jsonticketreopen', obj);
  }

  sendJsoncustomerComments(obj) {
    this.socket.emit('jsoncustomerquery', obj);
  }

  sendJsonTeamDetails(obj) {
    this.socket.emit('showteamdetail', obj);
  }

  sendJsonapllyleave(obj) {
    this.socket.emit('applyleave',  obj);
  }


  getConnection() {
    console.log('print chatbot service');
    this.socket = io(this.url, {
      query: {
        username: "Vaibhav",
        data: "Test User"
      }
    });

  }

  getMessages() {
    let observable = new Observable(observer => {
      this.socket.on('message', (data) => {
        data['serType'] = 'normal';
        observer.next(data);
      });

      this.socket.on('messagebot', (data) => {
        data['serType'] = 'normal';
        console.log("messagebot ::: " + JSON.stringify(data));
        observer.next(data);
      });

      this.socket.on('getuifields', (data) => {
        data['serType'] = 'c_inci';
        observer.next(data);
      });

      this.socket.on('ticket_cat_subcat', (data) => {
        data['serType'] = 'ticket_cat_subcat';
        data['serType'] = 'ticket_cat_subcat';
        data['dataStr'] = data;
        observer.next(data);
      });

      this.socket.on('loading', (data) => {
        data['serType'] = 'loading';
        observer.next(data);
      });


      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }

  public disconnectBot() {
    this.socket.disconnect();
  }

}
