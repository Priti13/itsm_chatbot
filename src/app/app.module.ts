import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChatbotComponent } from './chatbot/chatbot.component';

import { Router } from '@angular/router'
import { ActivatedRoute } from '@angular/router';


import * as $ from "jquery";
import { Observable, Subscription } from 'rxjs';
import { ChatbotService } from './chatbot/chatbot.service';
import {NewChatbotService} from "./new-chat-bot/new-chatbot.service"

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SpeechRecognitionService } from './speech-recognition/speech-recognition.service';
import {CustomInterceptor} from './interceptor/custom.interceptor'
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgDatepickerModule } from 'ng2-datepicker';
import { TimeFilterComponent } from '../time-filter/time-filter.component';
import { SharedServices } from './shared_/shared.services';
import { LayoutModule } from './layout/layout.module';
import { NewChatBotComponent } from './new-chat-bot/new-chat-bot.component';

// import { ModulesModule } from './modules/modules.module';
import {AuthGuard} from './AuthGaurd/auth.guard';



@NgModule({
  declarations: [
    AppComponent,
    ChatbotComponent,
    TimeFilterComponent,
    NewChatBotComponent,
    

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    NgbModule,
    NgDatepickerModule,
    LayoutModule
  ],
  providers: [ChatbotService, SpeechRecognitionService, SharedServices,NewChatbotService
    /*{
    provide:HTTP_INTERCEPTORS,
    useClass:CustomInterceptor,
    multi: true
  }*/
],
  bootstrap: [AppComponent]
})
export class AppModule { }

console.log('application module')
