import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CatsServiceComponent } from './cats-service/cats-service.component';
import { ServicesComponent } from './services/services.component';
import { CatsHomeComponent } from './cats-home/cats-home.component';
import { HardwareComponent } from './hardware/hardware.component';


const routes: Routes = [
  
    // { path:'catsService', component:CatsServiceComponent },
    // { path:'services',component:ServicesComponent   },
    // { path:'hardware',component:HardwareComponent },
    {path: 'home', component: CatsHomeComponent}

];

@NgModule({
    imports: [CommonModule, RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})


export class ModulesRoutingModule { }