import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatsServiceComponent } from './cats-service/cats-service.component';
import { ServicesComponent } from './services/services.component';
import { ModulesRoutingModule } from './modules.routing.module';
import { CatsHomeComponent } from './cats-home/cats-home.component';
import { HardwareComponent } from './hardware/hardware.component';

@NgModule({
  imports: [
    CommonModule,
    ModulesRoutingModule,
    
  ],
  declarations: [CatsServiceComponent, ServicesComponent, CatsHomeComponent,HardwareComponent]
})
export class ModulesModule { }
