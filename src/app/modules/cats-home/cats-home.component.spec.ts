import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CatsHomeComponent } from './cats-home.component';



describe('CatsServiceComponent', () => {
  let component: CatsHomeComponent;
  let fixture: ComponentFixture<CatsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
