import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CatsHomeComponent } from './cats-home.component';


const routes: Routes = [
  
    // { path:'', component:CatsHomeComponent   },

];

@NgModule({
    imports: [CommonModule, RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})


export class CatsServiceRoutingModule { }