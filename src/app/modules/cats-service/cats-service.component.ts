import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cats-service',
  templateUrl: './cats-service.component.html',
  styleUrls: ['./cats-service.component.css']
})
export class CatsServiceComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  
  servicesClick() {
    this.router.navigate(['/modules/services']);
  
  }

hardwareClick(){
  this.router.navigate(['/modules/hardware']);
}

}
