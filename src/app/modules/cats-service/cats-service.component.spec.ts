import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatsServiceComponent } from './cats-service.component';

describe('CatsServiceComponent', () => {
  let component: CatsServiceComponent;
  let fixture: ComponentFixture<CatsServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatsServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatsServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
