import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CatsServiceComponent } from './cats-service.component';


const routes: Routes = [
  
    { path:'', component:CatsServiceComponent   },

];

@NgModule({
    imports: [CommonModule, RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})


export class CatsServiceRoutingModule { }