import { Subscription } from 'rxjs/Subscription';
import { Subscriber } from 'rxjs/Subscriber';
import { TimeFilterService } from './time-filter.service.component';
import { Component, OnInit, NgModule } from '@angular/core';
import { Daterangepicker } from 'ng2-daterangepicker';
import { DaterangepickerConfig } from 'ng2-daterangepicker';



declare var moment: any;



@Component({
  selector: 'app-time-filter',
  templateUrl: './time-filter.component.html',
  styleUrls: ['./time-filter.component.css'],

})


export class TimeFilterComponent implements OnInit {

  public dateFrom: any;
  public dateTo: any;
  public timeZone: any;

  public dateFormat ="DD-MM-YYYY h:mm:ss";

  public tsSubscribers: Subscription;

  public timeInfo: any = {
    date: {
      start: moment().subtract(1, 'month').format(this.dateFormat),
      end: moment().format(this.dateFormat)
    },
    timestamp: {
      start: Date.parse(moment().subtract(1, 'month')),
      end: Date.parse(moment()),
    }
  };


 



  ngOnInit() {
    this.tsSubscribers = this.timeFilterService.getTimeFilterSubscriber().subscribe(timeobj => {
      this.timeInfo = timeobj;
    });

  }


  //new uI

  public dateInputs: any = [
    {
      start: moment().subtract(12, 'month'),
      end: moment().subtract(6, 'month')
    },
    {
      start: moment().subtract(9, 'month'),
      end: moment().subtract(6, 'month')
    },
    {
      start: moment().subtract(4, 'month'),
      end: moment()
    },
    {
      start: moment(),
      end: moment().add(5, 'month'),
    }
  ];

  /**
   *  will set date range by default
   */
  public mainInput = {
    start: moment().subtract(1, 'month'),
    end: moment()
  }

  public singlePicker = {
    singleDatePicker: true,
    showDropdowns: true,
    opens: "left"
  }

  public singleDate: any;
  public eventLog = '';


  constructor(private daterangepickerOptions: DaterangepickerConfig, private timeFilterService: TimeFilterService) {
    console.log("init TimeFilter Component....");
    
    this.daterangepickerOptions.settings = {
      locale: { format: this.dateFormat },
      alwaysShowCalendars: false,
      timePicker: true,
      // ranges: {
      //   'This Month': [moment.utc().startOf('month'), moment.utc()],
      //   'Last Month': [
      //     moment.utc(moment.utc().startOf('month')).subtract(1, 'month').startOf('month'),
      //    moment.utc(moment.utc().startOf('month')).subtract(1, 'month').endOf('month')
      //   ],
      //   'Last 3 Months': [
      //     moment.utc(moment.utc().startOf('month')).subtract(3, 'month').startOf('month'),
      //     moment.utc(moment.utc().startOf('month')).subtract(1, 'month').endOf('month')
      //   ],
      //   'Last 6 Months': [
      //     moment.utc(moment.utc().startOf('month')).subtract(6, 'month').startOf('month'),
      //     moment.utc(moment.utc().startOf('month')).subtract(1, 'month').endOf('month')
      //   ],
      //   'Last 12 Months': [
      //     moment.utc(moment.utc().startOf('month')).subtract(12, 'month').startOf('month'),
      //   moment.utc(moment.utc().startOf('month')).subtract(1, 'month').endOf('month')
      //   ],
      // }

      ranges: {
        'This Month': [moment(moment().startOf('month')).startOf('day'), moment()],
        'Last Month': [
         moment(moment(moment().startOf('month')).subtract(1, 'month').startOf('month')).startOf('day'),
         moment(moment().startOf('month')).subtract(1, 'month').endOf('month')
        ],
        'Last 3 Months': [
          moment(moment(moment().startOf('month')).subtract(3, 'month').startOf('month')).startOf('day'),
          moment(moment().startOf('month')).subtract(1, 'month').endOf('month')
        ],
        'Last 6 Months': [
          moment(moment(moment().startOf('month')).subtract(6, 'month').startOf('month')).startOf('day'),
          moment(moment().startOf('month')).subtract(1, 'month').endOf('month')
        ],
        'Last 12 Months': [
          moment(moment(moment().startOf('month')).subtract(12, 'month').startOf('month')).startOf('day'),
        moment(moment().startOf('month')).subtract(1, 'month').endOf('month')
        ],
      }

    };

    //this.singleDate = Date.now();
    this.singleDate = this.timeInfo.timestamp.end;
// var st_ = (moment().subtract(12, 'month'));
// var en_ = (moment().subtract(6, 'month'));
//     console.log("start : "+st_);
//     console.log("end : "+en_);
  



  }

  public selectedDate(value: any, dateInput: any) {
    console.log("Date selected  : " + JSON.stringify(dateInput) + "value : " + JSON.stringify(value));
    console.log("start : " + this.mainInput.start + " end: " + this.mainInput.end);
    dateInput.start = value.start;
    dateInput.end = value.end;

    let startDateInTimeStamp = Date.parse(value.start);
    console.log("startDateInTimeStamp", startDateInTimeStamp);
   // this.timeFilterService.changeStartDate("" + startDateInTimeStamp); // bind startDate to timeservice  
    let endDateInTimeStamp = Date.parse(value.end);
    console.log("startDateInTimeStamp", endDateInTimeStamp);
    //this.timeFilterService.changeEndDate("" + endDateInTimeStamp); // bind endDate to timeservice 
    
    this.setDatetoTimeServices(value.start,value.end,startDateInTimeStamp,endDateInTimeStamp);
  }

  public singleSelect(value: any) {
    this.singleDate = value.start;
  }

  public applyDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;
  }

  public calendarEventsHandler(e: any) {
    console.log(e);
    this.eventLog += '\nEvent Fired: ' + e.event.type;
  }


  public setDatetoTimeServices(dateStart, dateEnd, tsStart, tsEnd) {
    console.log("Filter :dS "+dateStart.format("DD-MM-YYYY h:mm:ss")+" dE: "+dateEnd+" tsS: "+tsStart+" tsE: "+tsEnd);
    this.timeFilterService.setTimeInfo(dateStart.format(this.dateFormat), dateEnd.format(this.dateFormat), tsStart, tsEnd);
  }




}
