import { Observable, Subscription } from 'rxjs/Rx';
import { Component, OnInit, OnDestroy, EventEmitter, ViewChild, Renderer, HostListener, ViewEncapsulation, AfterViewInit, OnChanges } from '@angular/core';
import { Router } from '@angular/router'
import { RouterModule, Routes, ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import * as io from "socket.io";
import { socket } from "socket.io";

// import { SpeechRecognitionService } from "./speech-recognition.service";
import { ChatbotService } from "./chatbot.service";

import * as $ from "jquery";
import { SpeechRecognitionService } from 'src/app/speech-recognition/speech-recognition.service';


@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./css/stylenewchatbot.css']
})
export class ChatbotComponent implements OnInit, AfterViewInit, OnChanges {

  public isloading: boolean = false;

  public messDelay: number = 1300;
  public inactiveSince: number = 0;
  public subscrNotifyInactivity: Subscription;

  public interval$ = null;
  public intervalCards$ = null;

  public isConnected: boolean = false;

  public lastMessRes_: any = {};

  public modMess_ = { userName: "", isAuthorize: true, isAudio: false, content: [], custCards: [], showCarts: false, typingEnable: true, notifyActivity: false, maxInactiveLimit: 60 };
  public chttxt_plcholder: string = "Type Your Message ....";
  public voices: SpeechSynthesisVoice[] = [];

  public formSubmission(obj, ndx, action) {
    if (action && action === 'thanksfunction') {
      this.thanksfunction(obj);
      this.AbortSpeak();
    } else if (action && action === 'mailfunction') {
      this.mailfunction(obj);
      this.AbortSpeak();
    } else if (action && action === 'sendmailfuntion') {
      this.sendmailfuntion(obj);
      this.AbortSpeak();
    } else if (action && action === 'createincidentfunction') {
      this.createincidentfuntion(obj);
      this.AbortSpeak();
    } else if (action && action === 'showopenfunction') {
      this.showopenfunction(obj);
      this.AbortSpeak();
    }
    else if (action && action === 'showclosefunction') {
      this.showclosefunction(obj);
      this.AbortSpeak();
    }
    else if (action && action === 'showbothfunction') {
      this.showbothfunction(obj);
      this.AbortSpeak();
    }
    else if (action && action === 'exitfunction') {
      this.exitfunction(obj);
      this.AbortSpeak();
    }
    else if (action && action === 'datetoday') {
      this.datetoday(obj);
      this.AbortSpeak();
    }
    else if (action && action === 'datelast2day') {
      this.datelast2day(obj);
    }
    else if (action && action === 'datelast1week') {
      this.datelast1week(obj);
      this.AbortSpeak();
    }
    else if (action && action === 'datelast1month') {
      this.datelast1month(obj);
    }
    else if (action && action === 'datealldata') {
      this.datealldata(obj);
      this.AbortSpeak();
    }

    else if (action && action === 'showincidentclass') {
      this.showincidentclass(obj);
      this.AbortSpeak();
    }
    else if (action && action === 'createticketclass') {
      this.createticketclass(obj);
      this.AbortSpeak();
    }
    else if (action && action === 'slainfoclass') {
      this.slainfoclass(obj);
      this.AbortSpeak();
    }
    else if (action && action === 'incidentinfoclass') {
      this.incidentinfoclass(obj);
      this.AbortSpeak();
    }
    else if (action && action === 'helpclass') {
      this.helpclass(obj);
      this.AbortSpeak();
    }
    else if (action && action === 'submitcustomerlist') {
      this.submitcustomerlist(obj, ndx, action);
      this.AbortSpeak();
    } else if (action && action === 'showonholdfunction') {
      this.showonholdfunction(obj, ndx, action);
      this.AbortSpeak();
    } else if (action && action === 'showcustawaitfunction') {
      this.showcustawaitfunction(obj, ndx, action);
      this.AbortSpeak();
    }
    else if (action && action === 'showallstatusfunction') {
      this.showallstatusfunction(obj, ndx, action);
      this.AbortSpeak();
    }
    else if (action && action === 'statusyes') {
      this.statusyes(obj, ndx, action);
      this.AbortSpeak();
    }

    else if (action && action === 'feedbackyes') {
      this.feedbackyes(obj, ndx, action);
      this.AbortSpeak();
    }
    else if (action && action === 'feedbackno') {
      this.feedbackno(obj, ndx, action);
      this.AbortSpeak();
    }
    else if (action && action === 'incidentfunction') {
      this.incidentfunction(obj, ndx, action);
      this.AbortSpeak();
    }

    else if (action && action === 'requestfunction') {
      this.requestfunction(obj, ndx, action);
      this.AbortSpeak();
    }

    else if (action && action === 'goodexperiencefunction') {
      this.goodexperiencefunction(obj, ndx, action);
      this.AbortSpeak();
    }
    else if (action && action === 'averageexperiencefunction') {
      this.averageexperiencefunction(obj, ndx, action);
      this.AbortSpeak();
    }
    else if (action && action === 'badexperiencefunction') {
      this.badexperiencefunction(obj, ndx, action);
      this.AbortSpeak();
    }

    else if (action && action === 'sendfeedbackcomment') {
      this.sendfeedbackcomment(obj);
      this.AbortSpeak();
    }


    else if (action && action === 'totalslafunction') {
      this.totalslafunction(obj, ndx, action);
      this.AbortSpeak();
    }
    else if (action && action === 'grossfcrfunction') {
      this.grossfcrfunction(obj, ndx, action);
      this.AbortSpeak();
    }
    else if (action && action === 'netfcrfunction') {
      this.netfcrfunction(obj, ndx, action);
      this.AbortSpeak();
    }
    else if (action && action === 'incidentslafunction') {
      this.incidentslafunction(obj, ndx, action);
      this.AbortSpeak();
    } else if (action && action === 'downloadlinkshowticket') {
      let link = obj.fields[ndx].model;
      window.open(link, "_blank");
      this.modMess_.notifyActivity = false;
      this.chatService.sendAfterDownload("ask questions after download..");
      this.stuckform(obj);
      this.AbortSpeak();
    }
    else if (action && action === 'downloadlink_statusyes') {
      this.sendMessToService('mail me');
      this.usrtoMachine('yes');
      this.stuckform(obj);
      this.scrollSet();
      this.AbortSpeak();
    } else if (action && action === 'downloadlink_statusno') {
      this.usrtoMachine('no');
      this.sendMessToService('EXIT_NO');
      this.stuckform(obj);
      this.scrollSet();
      this.AbortSpeak();
    }
    this.scrollSet();
  }


  public totalslafunction(obj, ndx, action) {
    this.sendMessToService('totalsla');
    this.usrtoMachine('Total SLA');
    this.AbortSpeak();
    this.stuckform(obj);
    this.scrollSet();
  }

  public grossfcrfunction(obj, ndx, action) {
    this.sendMessToService('grossfcr');
    this.usrtoMachine('Gross FCR.');
    this.AbortSpeak();
    this.stuckform(obj);
    this.scrollSet();
  }
  public netfcrfunction(obj, ndx, action) {
    this.sendMessToService('netfcr');
    this.usrtoMachine('Net FCR.');
    this.AbortSpeak();
    this.stuckform(obj);
    this.scrollSet();
  }
  public incidentslafunction(obj, ndx, action) {
    this.sendMessToService('incidentsla');
    this.usrtoMachine('Incident SLA.');
    this.AbortSpeak();
    this.stuckform(obj);
    this.scrollSet();
  }
  public sendfeedbackcomment(obj) {
    var fields: any[] = obj.fields;
    fields.forEach(fld => {
      if (this.validFormFType.indexOf(fld.type) > -1) {
        var vmodl_emailText = fld.model;
        this.chatService.sendFeedBackComments(vmodl_emailText);
        // this.usrtoMachine('Mail to :' + vmodl_emailText);
        this.stuckform(obj);
      }
    })
    this.clearChatWindow();
    this.scrollSet();
  }
  public goodexperiencefunction(obj, ndx, action) {
    let objFld = obj.fields[ndx];
    this.chatService.sendExperienceToChatBot(objFld.model);
    this.stuckform(obj);
    // this.clearChatWindow();
    this.scrollSet();
  }
  public averageexperiencefunction(obj, ndx, action) {
    let objFld = obj.fields[ndx];
    this.chatService.sendExperienceToChatBot(objFld.model);
    this.stuckform(obj);
    // this.clearChatWindow();
    this.scrollSet();
  }
  public badexperiencefunction(obj, ndx, action) {
    let objFld = obj.fields[ndx];
    this.chatService.sendExperienceToChatBot(objFld.model);
    this.stuckform(obj);
    // this.clearChatWindow();
    this.AbortSpeak();
    this.scrollSet();
  }
  public requestfunction(obj, ndx, action) {
    this.chatService.sendMessage('request');
    this.usrtoMachine('create service request');
    this.stuckform(obj);
    this.scrollSet();
  }
  public incidentfunction(obj, ndx, action) {
    this.chatService.sendMessage('incident');
    this.usrtoMachine('create incident');
    this.stuckform(obj);
    this.scrollSet();
  }
  public feedbackyes(obj, ndx, action) {
    let objFld = obj.fields[ndx];
    this.chatService.sendFeedBack(objFld.model);
    this.stuckform(obj);
    this.clearChatWindow();
    this.scrollSet();
  }
  public feedbackno(obj, ndx, action) {
    let objFld = obj.fields[ndx];
    this.chatService.sendFeedBack(objFld.model);
    this.stuckform(obj);
    // this.clearChatWindow();
    this.scrollSet();
  }
  public statusyes(obj, ndx, action) {
    this.sendMessToService('YES');
    this.usrtoMachine('yes');
    this.stuckform(obj);
    this.scrollSet();
  }
  public showallstatusfunction(obj, ndx, action) {
    this.sendMessToService('ALLSTATUS');
    this.usrtoMachine('All Status');
    this.stuckform(obj);
    this.scrollSet();
  }
  public showcustawaitfunction(obj, ndx, action) {
    this.sendMessToService('CUSTAWTD');
    this.usrtoMachine('Awaiting Customer Update');
    this.stuckform(obj);
    this.scrollSet();
  }
  public showonholdfunction(obj, ndx, action) {
    this.sendMessToService('ONHOLD');
    this.usrtoMachine('On Hold');
    this.stuckform(obj);
    this.scrollSet();
  }
  public submitcustomerlist(obj, ndx, action) {

    for (var i = 0; i < obj.fields.length; i++) {
      var obj_fld = obj.fields[i];
      if (obj_fld.id === 'customer') {
        var items = obj_fld.selectedoptions;

        let itemList = [];
        items.forEach(itm => {
          itemList.push(itm.id);
        });

        if (itemList.length > 0) {
          this.chatService.sendCustomerList(itemList);
          this.machineToUser('Customer Details');
          this.stuckform(obj);
          this.scrollSet();
        } else {
          alert("Please Select Customer");
        }


      }
    }


    // let list = ["altec","liquid"];
    // this.chatService.sendCustomerList(list);
    // this.machineToUser('Customer Details');
  }
  private showincidentclass(obj) {
    this.sendMessToService('show.Incident');
    this.usrtoMachine('Show Ticket Status');
    this.stuckform(obj);
    this.scrollSet();
  }
  private createticketclass(obj) {
    this.sendMessToService('create.incident');
    this.usrtoMachine('Create Ticket');
    this.stuckform(obj);
    this.scrollSet();
  }
  private slainfoclass(obj) {
    this.sendMessToService('sla.worklog');
    this.usrtoMachine('SLA Information');
    this.stuckform(obj);
    this.scrollSet();
  }
  private incidentinfoclass(obj) {
    this.sendMessToService('specific.incident');
    this.usrtoMachine('Incident Information');
    this.stuckform(obj);
    this.scrollSet();
  }
  private helpclass(obj) {
    this.sendMessToService('help.knowledge');
    this.usrtoMachine('Help');
    this.stuckform(obj);
    this.scrollSet();
  }
  private createincidentfuntion(obj) {
    var template = {}
    var fields: any[] = obj.fields;
    fields.forEach(fld => {
      if (this.validFormFType.indexOf(fld.type) > -1) {
        var vmodl_emailText = fld.model;
        var id = fld.id;
        template[id] = vmodl_emailText;
      }
    })
    this.chatService.sendMessageJOSN([template]);
    this.stuckform(obj);
    this.scrollSet();
  }
  private showopenfunction(obj) {
    this.sendMessToService('open');
    this.usrtoMachine('Show open tickets');
    this.stuckform(obj);
    this.scrollSet();
  }
  private showbothfunction(obj) {
    this.sendMessToService('both');
    this.usrtoMachine('Show both open and close tickets');
    this.stuckform(obj);
    this.scrollSet();
  }
  private showclosefunction(obj) {
    this.sendMessToService('closed');
    this.usrtoMachine('Show Closed tickets');
    this.stuckform(obj);
    this.scrollSet();
  }
  private exitfunction(obj) {
    this.sendMessToService('EXIT');
    // this.usrtoMachine('exit');
    this.stuckform(obj);
    this.clearChatWindow();
    this.scrollSet();
  }
  private datetoday(obj) {
    this.sendMessToService('TODAY');
    this.usrtoMachine('todays data');
    this.stuckform(obj);
    this.scrollSet();
  }
  private datelast2day(obj) {
    this.sendMessToService('LAST2DAY');
    this.usrtoMachine('Last 2 days data');
    this.stuckform(obj);
    this.scrollSet();
  }
  private datelast1week(obj) {
    this.sendMessToService('LAST1WEEK');
    this.usrtoMachine('Last 1 week data');
    this.stuckform(obj);
    this.scrollSet();
  }
  private datelast1month(obj) {
    this.sendMessToService('LAST1MONTH');
    this.usrtoMachine('Fetching Results For Last 1 Month');
    this.stuckform(obj);
    this.scrollSet();
  }
  private datealldata(obj) {
    this.sendMessToService('ALLDATA');
    this.usrtoMachine('All Data');
    this.stuckform(obj);
    this.scrollSet();
  }
  public thanksfunction(obj) {
    this.sendMessToService('thanks');
    this.usrtoMachine('thanks');
    this.stuckform(obj);
  }
  public mailfunction(obj) {
    this.sendMessToService('mail me');
    this.usrtoMachine('mail me');
    this.stuckform(obj);
  }
  public sendmailfuntion(obj) {
    var fields: any[] = obj.fields;
    fields.forEach(fld => {
      if (this.validFormFType.indexOf(fld.type) > -1) {
        var vmodl_emailText = fld.model;
        var regex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (vmodl_emailText && vmodl_emailText != '' && vmodl_emailText.match(regex)) {
          this.chatService.sendMessageMailList(vmodl_emailText);
          this.usrtoMachine('Mail to :' + vmodl_emailText);
          this.stuckform(obj);
        } else {
          this.chatBotSpeak("Please Fill A Valid Email ID...");
        }

      }
    })



    // Mail to : " + data
  }



  public validFormFType = ['input:text', 'input:number', 'input:email', 'select'];
  public modCreateIncident = {
    mappingcat: [],
    customerServiceMapp: []
  }




  ngAfterViewInit() {
    this.loadJSOnInit();
  }


  public toggleCh() {
    this.clearChatWindow();
    this.isConnected = !this.isConnected;
    if (!this.isConnected) {
      this.chatDisconnected();
      this.chatService.disconnectBot();
    } else {
      this.isloading = true;
      this.initSpeak();
    }
    if ($('#In2CB_chatdiv').hasClass('close')) {
      $('#In2CB_chatdiv').removeClass('close');
      $('#In2CB_chatdiv').addClass('open');
      this.openChat();
      this.scrollSet();
      this.voices = window.speechSynthesis.getVoices();
    } else if ($('#In2CB_chatdiv').hasClass('open')) {
      $('#In2CB_chatdiv').removeClass('open');
      $('#In2CB_chatdiv').addClass('close');
      //write functionality of close chat
      this.clearChatWindow();
      this.disconnect();
    }
    //$('#In2CB_chatdiv').toggle();
  }

  /**
   *  it will invoke when chat window is off.
   *  here all intervals will be delected.
   *  
   */
  public chatDisconnected() {
    this.clearMessageInterval();
    this.ClearIntervalCards();
    this.scrollSet();
    this.AbortSpeak();
  }


  async loadJSOnInit() {
    await this.loadScript('assets/chatbot/js/jquery.min.js');
    await this.loadScript('assets/chatbot/js/index.js');
    await this.loadScript('assets/chatbot/js/jquery-2.2.4.min.js');
  }

  public loadScript(scriptUrl: string) {
    return new Promise((resolve, reject) => {
      const scriptElement = document.createElement('script');
      scriptElement.src = scriptUrl;
      scriptElement.onload = resolve;
      document.body.appendChild(scriptElement);
    })
  };

  public boatCardClk(crdObj_) {
    var messtoSend = crdObj_.action;
    var messtoDisplay = crdObj_.displayMess;
    this.sendMessToService(messtoSend);
    this.usrtoMachine(messtoDisplay);
    this.modMess_.showCarts = !this.modMess_.showCarts;
    this.scrollSet();
    this.AbortSpeak();
  }


  /**
   * 
   * @param sel 
   * @param obj 
   * @param obsec 
   * @param action 
   */
  public onDynamicDDChange(sel, obj, obsec, action) {
    /**
     *  manage  category and subcategory mapping info .....
     */
    if (action && action == 'selectcatagorycreateincident') {
      let sCatDD = this.getSubCateCreateIncidnt(sel);
      for (var i = 0; i < obj.length; i++) {
        if (obj[i].id === 'subcatagory') {
          obj[i].options = sCatDD;
        }
      }
    }
    /**
     *  manage  customer and service IDs mapping info .....
     */
    else if (action && action == 'selectserviceid') {
      let cSerDD = this.getServiceIdCreateIncident(sel.toLowerCase());
      for (var i = 0; i < obj.length; i++) {
        if (obj[i].id === 'serviceId') {
          obj[i].options = cSerDD;
        }
      }
    }
  }


  public getServiceIdCreateIncident(name): any[] {
    let serviceIds = [{
      key: "Select Service Id", value: ""
    }]
    for (var i = 0; i < this.modCreateIncident.customerServiceMapp.length; i++) {
      if (this.modCreateIncident.customerServiceMapp[i].customerName === name) {
        for (var j = 0; j < this.modCreateIncident.customerServiceMapp[i].serviceIdList.length; j++) {
          serviceIds.push(this.modCreateIncident.customerServiceMapp[i].serviceIdList[j]);
        }
        return serviceIds;
      }
    }
    return serviceIds;
  }


  public getSubCateCreateIncidnt(name): any[] {
    let subCate = [{
      key: "Select Sub-Category", value: ""
    }]
    for (var i = 0; i < this.modCreateIncident.mappingcat.length; i++) {
      if (this.modCreateIncident.mappingcat[i].catagoryName === name) {
        for (var j = 0; j < this.modCreateIncident.mappingcat[i].subCatagoryList.length; j++) {
          subCate.push({
            key: this.modCreateIncident.mappingcat[i].subCatagoryList[j],
            value: this.modCreateIncident.mappingcat[i].subCatagoryList[j]
          })
        }
        return subCate;
      }
    }
    return subCate;
  }


  public stuckform(obj) {
    var fields: any[] = obj.fields;
    fields.forEach(fld => {
      if (fld.hasOwnProperty('editable')) {
        fld.editable = false;
      }
      if (fld.hasOwnProperty('required')) {
        fld.required = false;
      }
    })

  }

  public mousedown = new EventEmitter<MouseEvent>();
  public speaking: boolean = false;
  public showTabBtn = false;
  public currentChatHtml = "";
  public c_ = 0;
  public messages = [];
  public connection;
  public temp;
  public idList: any = [];
  public speechData: string;
  public speeker = true;
  public showCarts = true;

  scrollSet() {
    var container = $('#In2CB_chatBoxMainContainer')[0];
    var containerHeight = container.clientHeight;
    var contentHeight = container.scrollHeight;
    let scrollupTo = $(".messages")[0].scrollHeight;// contentHeight - containerHeight;
    let idlen = $(".messages")[0].scrollHeight;
    $("#In2CB_chatBoxMainContainer").stop().animate({ scrollTop: $("#In2CB_chatBoxMainContainer")[0].scrollHeight }, 1000);
  }

  constructor(private router: Router,
    private route: ActivatedRoute,
    private http: Http,
    private chatService: ChatbotService,
    private renderer: Renderer,
    private speechRecognitionService: SpeechRecognitionService,

  ) {
    this.speechData = "";
    this.isConnected = false;
  }


  public sendMessage() {
    var msztype = $('#myMessage').val();
    if (msztype.trim() != "") {
      this.usrtoMachine(msztype);
      $('#myMessage').val('');
      this.sendMessToService(msztype);
    }
  }

  public sendMessToService(mess_) {
    this.chatService.sendMessage(mess_);
    this.scrollSet();
  }

  ngOnInit() { }

  public startNotifyInactivity() {
    this.inactiveSince = 0;
    let timer = Observable.timer(1000, 1000);
    this.subscrNotifyInactivity = timer.subscribe(t => {
      this.getsubscrNotifyInactivity();
    });

  }

  public getsubscrNotifyInactivity() {
    if (this.modMess_.notifyActivity) {
      this.inactiveSince += 1;
      if (this.inactiveSince >= this.modMess_.maxInactiveLimit) {
        this.notifyLimitExceedsOpr();
      }
    } else {
      this.inactiveSince = 0;
      this.stopNotifyInactivity();
    }
  }

  public notifyLimitExceedsOpr() {
    if (this.inactiveSince == 15) {
      //send first message
      let inactive_warning1 = {
        userName: "", isAuthorize: true, isAudio: true, content: [
          {
            "src": "mcn", "type":
              "textmessage", "value": 'So have you decided your answer or should i proceeds'
          }
        ], custCards: [], showCarts: false, typingEnable: false, notifyActivity: true, maxInactiveLimit: 60
      };
      this.machineToUser(inactive_warning1);
    } else if (this.inactiveSince == 25) {
      this.chatService.sendInActiveMessage("inactive since 25 secounds");
      if ((this.modMess_.content.length > 1) && (this.modMess_.content[this.modMess_.content.length - 2].type == 'form')) {
        this.stuckform(this.modMess_.content[this.modMess_.content.length - 2]);
      }
    } else if (this.inactiveSince > 26) {
      this.stopNotifyInactivity();
    }
  }

  public stopNotifyInactivity() {
    if (!this.subscrNotifyInactivity.closed) {
      this.subscrNotifyInactivity.unsubscribe();
    }
  }



  public socket(message) {
    this.machineToUser(message);
    this.scrollSet();
  };

  ngOnDestroy() {
    this.AbortSpeak();
    this.clearChatWindow();
    this.speechRecognitionService.DestroySpeechObject();
    this.chatBotSpeak('');
    this.isloading = false;
  }

  public disconnect() {
    this.chatService.disconnectBot();
  }

  // Speak Machine to user message.
  public chatBotSpeak(message) {
    if (this.speeker && this.isConnected) {
      if ('speechSynthesis' in window) {
        var msg = new SpeechSynthesisUtterance(message);
        this.voices = window.speechSynthesis.getVoices();
        if (this.voices.length > 0) {
          // msg.voice = this.voices[3];
          msg.voice = this.voices[8];
          // msg.lang = "en-GB";
          msg.rate = 1;
        }
        window.speechSynthesis.speak(msg);
      }
    }
  }

  public initSpeak() {
    if ('speechSynthesis' in window) {
      var msg = new SpeechSynthesisUtterance('');
      this.voices = window.speechSynthesis.getVoices();
      if (this.voices.length > 0) {
        msg.voice = this.voices[8];
        msg.volume = 12;
        // msg.voice = this.voices[3];
        // msg.lang = "en-GB";
        msg.rate = 1;
      }
      window.speechSynthesis.speak(msg);
    }
  }


  public AbortSpeak() {
    if (this.speeker) {
      window.speechSynthesis.cancel();
    }
  }



  /**
   *    this f/n will open chat window
   */

  public openChatWindow() {
    $('#qnimate').addClass('popup-box-on');
    $("#removeClass").click(function () {
      $('#qnimate').removeClass('popup-box-on');
    });
    this.showTabBtn = !this.showTabBtn;
  }

  /**
   * 
   * @param message message (send message to chatbot)
   */
  public usrtoMachine(message) {
    this.modMess_.content.push({
      src: "usr",
      type: "textmessage",
      value: message
    });
    this.scrollSet();
    return this.currentChatHtml;
  }


  public counterTemp = 0;
  /**
   * 
   * @param message messageobj (retrieve message from chatbot)
   */
  public machineToUser(message) {
    this.counterTemp = 0;
    if (message && message.content) {
      this.validateInterval(message);
    }
    this.scrollSet();
    return this.currentChatHtml;
  }

  public validateInterval(message) {
    let count = 0;
    if (message.content && message.content.length > 0) {
      // const interval =    this.interval$
      const interval = window.setInterval(() => {
        this.manageLoader(message, count);
        count++;
        this.isloading = true;
        if ((count >= message.content.length) || (!this.isConnected)) {
          this.isloading = false;
          // window.clearInterval(interval);
          window.clearInterval(interval);
        }
      }, this.messDelay);
    }

  }

  public clearMessageInterval() {
    if (this.interval$ && this.interval$ != null) {
      console.log("Clear Intervals ................... ");
      window.clearInterval(this.interval$);
    }
  }


  public ClearIntervalCards() {
    if (this.intervalCards$ && this.intervalCards$ != null) {
      console.log("Clear Interval Cards ................... ");
      window.clearInterval(this.intervalCards$);
    }
  }

  public manageLoader(message, i) {
    this.isloading = false;
    if (this.speeker && message.isAudio && message.content[i].type === 'textmessage') {
      if (this.isConnected) {
        this.chatBotSpeak(message.content[i].value);
      } else {
        console.log("Can't Connect");
      }

    }
    this.modMess_.content.push(message.content[i]);
    this.scrollSet();
  }

  public manageIndicator(sts) {
    if (sts == 'close') {
      this.isloading = false;
    } else if (sts == 'open') {
      this.isloading = true;
    }
  }

  public clearChatWindow() {
    this.modMess_ = { userName: "", isAuthorize: true, isAudio: false, content: [], custCards: [], showCarts: false, typingEnable: false, notifyActivity: false, maxInactiveLimit: 60 };
  }


  /**
   * used for send message with keycode=13(enter key).
   * @param event
   */
  public handleKeyDown(event: any) {
    if (event.keyCode == 13) {
      this.sendMessage();
    }
  }



  /**
   *  it will open chat window &
   *  connect with chatbot server
   */

  public openChat() {
    this.showTabBtn = !this.showTabBtn;
    this.openChatWindow();
    this.connection = this.chatService.getConnection();
    this.chatService.getMessages().subscribe(message => {
      this.validateMsz(message);
    })

  }

  /**
   * 
   * @param message  keep incident cate&subCate mapping
   */
  public validateMsz(message) {
    this.modMess_.notifyActivity = message.notifyActivity;
    this.modMess_.maxInactiveLimit = message.maxInactiveLimit;
    if (this.modMess_.notifyActivity) {
      this.startNotifyInactivity();
    }


    if (message.serType == 'loading') {
      if (message.duration && message.duration > 0) {
        this.manageIndicator('open');
        setTimeout(() => {
          this.manageIndicator('open');
        }, ((this.messDelay) + 10) * (message.duration));
      } else {
        let lastContents = (this.lastMessRes_.content && this.lastMessRes_.content.length > 0) ? this.lastMessRes_.content.length : 1;
        this.manageIndicator('open');
        setTimeout(() => {
          this.manageIndicator('open');
        }, (((this.messDelay) + 10) * (lastContents)));
      }
    } else if (message.serType == 'c_inci') {
      this.modCreateIncident.mappingcat = message.mapping;
      this.modCreateIncident.customerServiceMapp = message.customerServiceMapping;

      this.modMess_.typingEnable = message.typingEnable;
      this.lastMessRes_ = message;
      this.messages.push(message);
      this.socket(message);
    } else if (message.custCards && message.custCards.length > 0 && message.showCarts) {
      let lastContents = (this.lastMessRes_.content && this.lastMessRes_.content.length > 0) ? this.lastMessRes_.content.length : 1;
      this.modMess_.typingEnable = message.typingEnable;

      this.intervalCards$ = window.setInterval(() => {
        if (this.isConnected) {
          this.modMess_.custCards = message.custCards;
          this.modMess_.showCarts = message.showCarts;
          this.scrollSet();
        }
        window.clearInterval(this.intervalCards$);

      }, ((this.messDelay) * (lastContents + 2)));


      this.lastMessRes_ = message;
      this.messages.push(message);
      this.socket(message);
    } else if (message.serType == 'normal') {
      this.modMess_.typingEnable = message.typingEnable;
      this.modMess_.showCarts = message.showCarts;
      this.lastMessRes_ = message;
      this.messages.push(message);
      this.socket(message);
    }

  }


  /**
   *   will invoke when user minimize chat window
   */
  public closeChat() {
    this.showTabBtn = !this.showTabBtn;
    //  this.cleanChatWindow();
    this.idList = [];
    this.connection.unsubscribe();

    // need to disconnect connection .....
  }

  public onChange(value: any) {
  }

  /**
   * 
   * @param sts true: blink false:stop blinking
   */
  public makeMicrophoneBlink(sts) {
    if (sts) {
      console.log("hasclass ... : " + (($('#microphone').hasClass('speak'))));
      if (!($('#microphone').hasClass('speak'))) {
        $('#microphone').addClass('speak');
      }
    } else {
      if ($('#microphone').hasClass('speak')) {
        $('#microphone').removeClass('speak');
      }
    }
  }


  /*
    Speech recogitions code... calling from SpeechRecognitionService
   */
  activeSpeechSearch(): void {
    this.chttxt_plcholder = "Please Speak ....."
    this.makeMicrophoneBlink(true);
    this.speechRecognitionService.record()
      .subscribe(
        //listener
        (value) => {

          this.chttxt_plcholder = "Bot is Listining....."
          this.speechData = value;
          if (this.speechData && (this.speechData != "")) {
            this.usrtoMachine(this.speechData);
            this.chatService.sendMessage(this.speechData);
          }
        },
        //errror
        (err) => {
          console.log(err);
          if (err.error == "no-speech") {
            console.log("--restatring service--");
            this.activeSpeechSearch();
          }
          this.chttxt_plcholder = "Can't Recognise Your Voice ...";
        },
        //completion
        () => {
          console.log("--complete--" + this.speechData);
          this.chttxt_plcholder = "Type Your Message ....";
          this.makeMicrophoneBlink(false);
        });
  }

  @HostListener('mouseDown', ['$event'])
  mouseDown(event) {
    console.log("onmouse down.........");
    this.activeSpeechSearch();
  }


  public toggleVoiceMute() {
    this.speeker = !(this.speeker);
    if (!this.speeker) {
      window.speechSynthesis.cancel();
    }
  }


  public getMessageLiCSS(obj) {
    if (obj.src == 'usr') {
      return 'message col-xs-9 pull-right ';
    } else if (obj.src == 'mcn') {
      return 'message col-xs-9 pull-left  ';
    }
  }

  ngOnChanges() {
    console.log("changes occure ");
  }

  public manageMszBoxDiv() {
    let position = 0;
    // console.log("manageMszBoxDiv....");
    if (this.modMess_.showCarts && this.modMess_.custCards.length > 0) {
      // $('#mszboxDiv').style.bottom = "225px";

      let shand = document.getElementsByClassName('inputContainter') as HTMLCollectionOf<HTMLElement>;
      if (shand.length != 0) {
        shand[0].style.bottom = "0px";//"218px";
      }

    } else {
      let shand = document.getElementsByClassName('inputContainter') as HTMLCollectionOf<HTMLElement>;
      if (shand.length != 0) {
        shand[0].style.bottom = "0px";
      }
    }

    return true;
  }



  /** handle multiselect  dropdown  */

  onSingleSelect(event, flds) {
    // console.log("onSingleSelect : " + event + " :fileds : " + JSON.stringify(flds));
  }

  onSingleDeSelect(event) {
    // console.log("onSingleDeSelect : " + event);
  }
  onSelectAll(event) {
    // console.log("onSelectAll : " + event);
  }
  onDeSelectAll(event) {
    // console.log("onDeSelectAll : " + event);
  }


}
