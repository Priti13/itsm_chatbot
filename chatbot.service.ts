import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { ElementRef, Injectable, ViewChild } from '@angular/core';
import $ from "jquery";


import * as io from 'socket.io-client';


@Injectable()
export class ChatbotService {

  public url = 'http://172.16.11.123:5001';//'http://41.160.253.188:5001';//"http://41.160.253.188:5001";  //'http://172.16.11.123:5001';//'http://172.16.11.101:5001';//  "http://172.16.10.66:5000"; //'http://catsdashboard.in2ittech.com:5000';
  public socket;

  public catagoryName: any = [];
  public subCatagoryList = "";
  public objDate = Date.now();



  // @ViewChild('submitIncident') submitIncident: ElementRef;


  sendMessage(message) {
    this.socket.send(message);
  }

  sendMessageMailList(message) {
    this.socket.emit('maillist', message);
  }

  sendFeedBackComments(message) {
    this.socket.emit('feedbackComments', message);
  }

  sendInActiveMessage(message) {
    this.socket.emit('sendInActive', message);
  }

  sendAfterDownload(message) {
    this.socket.emit('afterDownload', message);
  }


  sendFeedBack(message) {
    this.socket.emit('feedback', message);
  }

  sendExperienceToChatBot(message) {
    this.socket.emit('experience', message);
  }


  sendCustomerList(message: any) {
    this.socket.emit('custfilter', message);
  }


  sendMessageJOSN(obj) {
    this.socket.emit('jsoncreateincident', obj);

  }

  submit(message) {
    this.socket.emit(message);
  }

  public checkAuth() {
    var res = {
      'isAuthValid': false,
      'data': ''
    }

    try {
      var currentUser = JSON.parse(localStorage.getItem('currentUser'));
      var loginName = localStorage.getItem("loginName");
      if (currentUser && currentUser.access_token != "") {
        var authJSON = {
          'authToken': currentUser.access_token,
          'full_name': loginName,
          'user_role': 'normaluser',
        };
        res.isAuthValid = true;
        res.data = JSON.stringify(authJSON);
      }
    } catch (e) {
      res.isAuthValid = false;
    }
    return res;
  }


  getConnection() {
    var loginName = localStorage.getItem("loginName");
    var Authres = this.checkAuth();
    if (Authres.isAuthValid) {
      this.socket = io(this.url, {
        query: {
          username: loginName,
          data: Authres.data
        }
      });
    }
  }
  getMessages() {
    let observable = new Observable(observer => {
      this.socket.on('message', (data) => {
        data['serType'] = 'normal';
        observer.next(data);
      });

      this.socket.on('messagebot', (data) => {
        data['serType'] = 'normal';
        console.log("messagebot ::: " + JSON.stringify(data));
        observer.next(data);
      });

      this.socket.on('getuifields', (data) => {
        data['serType'] = 'c_inci';
        observer.next(data);
      });

      this.socket.on('loading', (data) => {
        data['serType'] = 'loading';
        observer.next(data);
      });


      return () => {
        this.socket.disconnect();
      };
    });
    return observable;
  }


  public disconnectBot() {
    this.socket.disconnect();
  }



  /*
  getting category and sub category for the create incident
*/
  public getMCatOptionString(catagory) {
    var finalStr = "";
    for (let i = 0; i < catagory.length; i++) {
      var optGroupStr = '';
      optGroupStr = ' <optgroup  label=' + catagory[i].catagoryName + ' value=' + catagory[i].catagoryName + ' ng-reflect-model="true"  _ngcontent-c1 id=' + i + '_' + this.objDate + '>';
      var optionStr = "";
      for (let j = 0; j < catagory[i].subCatagoryList.length; j++) {
        optionStr = optionStr + '<option style="max-height:50px !important " value=' + catagory[i].subCatagoryList[j] + '>' + catagory[i].subCatagoryList[j] + '</option>';
      }
      optGroupStr = optGroupStr + optionStr + '</optgroup>';
      finalStr = finalStr + optGroupStr;
    }

    return finalStr;
  }


  public incidentdata(id) {
    var dataobj = [{
      'catagory': $('#category_' + (id.split('_')[1])).val(),
      'subject': $('#subject_' + (id.split('_')[1])).val(),
      'description': $('#description_' + (id.split('_')[1])).val(),
      'customer': $('#customer_' + (id.split('_')[1])).val(),
      'incidentdata': $('#incidentdata_' + (id.split('_')[1])).val()
    }]
    this.sendMessageJOSN(dataobj);
  }


}
